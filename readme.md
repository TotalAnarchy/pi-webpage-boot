# RaspberryPi opening a webpage at pi boot

### **Very basic** Using a [RaspberryPi][rpi-link] to display a webpage automatically at boot.

> **Required materials:**
> ---
> * (1) RaspberryPi of your choice (I used a 3B that was laying around) & accessories as needed (charger/ethernet cable/etc)
>



> **Step 1: Basic install of *Raspbian***
> ---
> (I used *Raspbian Lite* but should work with any distro of your choice)
> * Download latest *Raspbian Full* version from [RaspberryPi website][raspbian-link]
> * Flash it to a SD card using [*Balena Etcher*][etcher-link] (or whatever flasher you prefer)
> * Create a blank file simply named ssh (no extension) on the boot folder of the SD card to enable SSH by default
> * Boot the pi with the new SD card
> * Find the IP (recommended to set a reservation on the router to always get the same IP) and SSH into it. (default user/pass is *`pi`/`raspberry`*)
> * Run `sudo raspi-config` and change the following:
>   * Hostname of the pi
>   * *pi* user default password
>   * WiFi Country / SSID / password
>   * Enable VNC *(optional)*
> * Run `sudo apt update && sudo apt upgrade -y` to update your pi and packages to latest versions
> * Run `sudo reboot now`



> **Step 2: Auto start Chromium in kiosk mode (fullscreen)**
> ---
> * Run `nano ~/.config/lxsession/LXDE-pi/autostart`
> * Add the following:
>   > `#Disable screensaver/screen blanking`  
>	> `@xset s off`   
>	> `@xset -dpms`  
>	> `@xset s noblank`  
>	>  	
>	> `#Auto-start Chromium`  
>	> `@chromium-browser --noerrdialogs --kiosk http://*your_url_here*/`
> * Save and exit



> **Step 3: Set auto reboot times to keep things working smoothly**
> ---
> * Run `sudo crontab -e`
> * Add the following lines after existing text *(reboot hours suited my needs, change as you feel necessary)*:
>   > `#Auto reboot twice a day at 7:00 and at 13:30`  
>	> `0 7 * * *	/sbin/shutdown -r now`  
>	> `30 13 * * *	/sbin/shutdown -r now`  
>	> `#(optional) update pi every sunday at 3:00 (uncomment to enable)`  
>	> `#0 3 * * 0	apt update && apt upgrade -y`
> * Save and exit



> **Step 4: Reboot and check everything works**
> ---
> * Run `sudo reboot now`
> * Check everything runs as you want



> **Optional: Using Firefox instead of Chromium**
> ---
> * Run `sudo apt install -y firefox-esr`
> * VNC into the pi, open Firefox and:
>   * Set homepage to `http://*your_url_here*/`
>   * Install `auto-fullscreen` extension
>   * Go to `about:config` , search `browser.session.resume_from_crash` , set to false
>   * Revise other Firefox configs as needed
>   * Exit Firefox & VNC
> * In the SSH terminal, run `nano ~/.config/lxsession/LXDE-pi/autostart` and replace `@chromium-browser --noerrdialogs --kiosk http://*your_url_here*/` with `@firefox-esr`





[rpi-link]: https://www.raspberrypi.org
[raspbian-link]: https://www.raspberrypi.org/downloads/raspbian/
[etcher-link]: https://www.balena.io/etcher/
